function refTracker( obj ) {
	if ( typeof obj.target === 'function' ) {
		console.log( 'refTracker length: ' + refTracker.refs.length );
		refTracker.refs.push( [new obj.target(), obj.name, obj.instance] );
	};
};
refTracker.refs = [];

function staticVars() {
	if ( !( this instanceof staticVars ) ) {
		return new staticVars();
	};
	staticVars.instances = ++staticVars.instances || 1;
	console.log( 'Instances: ' + staticVars.instances );
};

refTracker( {'target' : staticVars, 'name' : 'one', 'instance' : ( function() { return refTracker.refs.length || 0 }() ) } );
refTracker( {'target' : staticVars, 'name' : 'two', 'instance' : ( function() { return refTracker.refs.length || 0 }() ) } );
refTracker( {'target' : staticVars, 'name' : 'ten', 'instance' : ( function() { return refTracker.refs.length || 0 }() ) } );


for( var i = 0, l = refTracker.refs.length; i < l; i++ ) {
    if ( refTracker.refs[i][1] === 'one' ) {
        console.log('found and removed');
        delete refTracker.refs[i];
        break;
    };
};

define( function( require ) {
	var canvas_ex1.js = require('./')
} );