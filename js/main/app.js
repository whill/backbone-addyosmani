function refTracker( obj ) {
	if ( typeof obj.target === 'function' ) {
		console.log( 'refTracker length: ' + refTracker.refs.length );
		refTracker.refs.push( [new obj.target(), obj.name, obj.instance] );
	};
};
refTracker.refs = [];

function staticVars() {
	if ( !( this instanceof staticVars ) ) {
		return new staticVars();
	};
	staticVars.instances = ++staticVars.instances || 1;
	console.log( 'Instances: ' + staticVars.instances );
};

refTracker( {'target' : staticVars, 'name' : 'one', 'instance' : ( function() { return refTracker.refs.length || 0 }() ) } );
refTracker( {'target' : staticVars, 'name' : 'two', 'instance' : ( function() { return refTracker.refs.length || 0 }() ) } );
refTracker( {'target' : staticVars, 'name' : 'ten', 'instance' : ( function() { return refTracker.refs.length || 0 }() ) } );


for( var i = 0, l = refTracker.refs.length; i < l; i++ ) {
    if ( refTracker.refs[i][1] === 'one' ) {
        delete refTracker.refs[i];
        console.log('found and removed');
        break;
    };
};

/* Basic paths config. A root can be specified as well */
require.config( {
	paths : {
		'jquery' : '//ajax.googleapis.com/ajax/libs/jquery/ne1.10.1/jquery.min',
		'underscore' : '../lib/underscore',
		'canvas_ex1' : '../canvas/canvas_ex1',
		'canvas_ex2' : '../canvas/canvas_ex2',
		'bind_to_canvas_and_draw' : '../canvas/bind_to_canvas_and_draw'
	}
} );

/* initialize our canvas by requiring "module: canvas_ex2". Module canvas_ex2 has the following dependency: canvas_ex1 */
/*
require( ['canvas_ex2'], function( obj ) {
	console.log( obj );
	obj.init();
} );
*/

/* Bind to our canvasDiv and render the Lilly background */
require( ['bind_to_canvas_and_draw'], function( obj ) {
	console.log( obj );
	obj.init();
} );