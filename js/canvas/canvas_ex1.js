define( ['jquery', 'underscore'], function( $, _ ){
	var
		/*
		setName = function( name ) {
			myName = name;
		},
		showName = function() {
			return myName;
		},
		myName = 'no name';
		subModules = {},
		subModules['show'] = showName,
		subModules['set'] = setName,
		*/
		exports = {},
		canvasEl = document.createElement('canvas'),
		canvasDocFragment = document.createDocumentFragment();

	canvasEl.setAttribute( 'id', 'canvasEl' );
	canvasEl.style.height = '900px';
	canvasEl.style.width = '1440px';
	canvasEl.style.border = '2px solid #000';

	canvasDocFragment.appendChild( canvasEl );
	exports.canvasEl = canvasDocFragment;

	return exports;
} );