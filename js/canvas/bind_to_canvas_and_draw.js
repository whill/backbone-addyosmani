define( ['canvas_ex2'], function( cEx2 ){
	var
		exports = {
			init : function() {
				cEx2.init();
				var
					canvas = document.getElementById( 'canvasEl' ),
					context = canvas.getContext( '2d' ),
					imageArray = [],
					imageNames = ['images/lucky-charms-wallpaper.jpg', 'images/lucky-charms-wallpaper.jpg', 'images/tossingtheline-wallpaper.jpg'];

				for( var i = 0, l = imageNames.length; i < l; i++ ) {
					imageArray[i] = new Image();
					imageArray[i].src = imageNames[i];
					(function( ele ){
						ele.onload = function( e ) {
							console.log( ele );
							context.drawImage( ele, 0, 0 );
						};
					}(imageArray[i]));
				};
			}
		};
	return exports;
} );